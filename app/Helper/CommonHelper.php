<?php

use App\Library\EmployeeDetailsLib as EmpDetails;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\District;
use App\AuditTrail;

/* ------------------------------------------------------------------------------
 * Calculate age from date of birth
 * -----------------------------------------------------------------------------
 * 
 * This function is use for calculate age from given date of birth
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if(!function_exists("dob2age")){
    function dob2age($dob)
    {
        $years = Carbon::parse($dob)->age;
        return $years;
    }
}


/* ------------------------------------------------------------------------------
 * Conver given age to dob
 * -----------------------------------------------------------------------------
 * 
 * This function is use to convert given age to dob
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if(!function_exists("age2dob")){
    function age2dob( $years ){
        return date('Y-m-d', strtotime($years . ' years ago'));
        }
}

/* ------------------------------------------------------------------------------
 * Save logs for further use
 * -----------------------------------------------------------------------------
 * 
 * This function is use to save logs
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if(!function_exists("addTrail")){
    function addTrail($description,$type)
    {
        AuditTrail::create(array(
            'ip'=>Request::ip(),
            'access_by'=>Auth::user()->email,
            'description'=>$description,
            'type'=>$type,
            'created_at'=>date('Y-m-d H:i:s')
        ));
    }
}



if(!function_exists("checkMobile")){
    function checkMobile($email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match("/^[0-9]{11}$/", $email)){
			return true;
		}
    }
}

/* ------------------------------------------------------------------------------
 * Select district name for present or permanent/home district
 * -----------------------------------------------------------------------------
 * 
 * This function is use to district name for or permanent/home district
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if(!function_exists("presentDistrict")){
    function presentDistrict($id)
    {
        $district = District::select('english_name')->where('id', $id)->first();
        return $district->english_name;
    }
}

if(!function_exists("homeDistrict")){
    function homeDistrict($id)
    {
        $district = District::select('english_name')->where('id', $id)->first();
        return $district->english_name;
    }
}



/* ------------------------------------------------------------------------------
 * Calculate age from date
 * -----------------------------------------------------------------------------
 * 
 * This function is use for calculate age from given date of birth
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if(!function_exists("dob2age")){
    function dob2age($dob)
    {
        $years = Carbon::parse($dob)->age;
        return $years;
    }
}


/* ------------------------------------------------------------------------------
 * Convert cm to feet and inches
 * -----------------------------------------------------------------------------
 * 
 * This function is use for convert cm value to feet and inches
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if(!function_exists("inches2feet")){
    function inches2feet($inches)
    {        
        $feet = intval($inches/12);
        $inches = $inches%12;
        return sprintf('%d ft %d ins', $feet, $inches);
    }
}

if(!function_exists("feet2inches")){
    function feet2inches($feet, $inches)
    {
        $inches = ($feet * 12) + $inches;
        //$inches = $inchesFromFeet + $inches;        
        return $inches;
    }
}

/* ------------------------------------------------------------------------------
 * Verify User
 * -----------------------------------------------------------------------------
 * 
 * This function is use for verify user with verification token
 * 
 * 
 * @author: Masud Rana
 * @email: masud.rana@halinindustries.com
 */
if (!function_exists("user_verify"))
{

    function user_verify($token, $code) {
        if($code === $token){
            return true;
        }else{
            return false;
        }
    }

}


/* ------------------------------------------------------------------------------
 * Send Email with verification code.
 * -----------------------------------------------------------------------------
 * 
 * This function is use for send an email with account verification code.
 * 
 * 
 * 
 * @author: Masud Rana
 * @email: masudrana.iubat@gamil.com
 */
if (!function_exists("sendEmail"))
{

    function sendEmail($data) {
        Mail::send('pages.users.email.verification', $data, function($message) use ($data){
            $message->from('masud.rana@halimindustries.com');
            $message->to($data['email'], $data['name'])->subject($data['subject']);
        });
    }
}


/* ------------------------------------------------------------------------------
 * Send SMS with verification code.
 * -----------------------------------------------------------------------------
 * 
 * This function is use for send a sms with account verification code.
 * 
 * 
 * 
 * @author: Masud Rana
 * @email: masudrana.iubat@gamil.com
 */
if (!function_exists("sendSms"))
{

    function sendSms($mobile, $message) {
        $user = "dohs";
        $pass = "D0hs2019";
        $sid = "BanaiDOHSEng";
        $url="http://sms.sslwireless.com/pushapi/dynamic/server.php";
        $param="user=$user&pass=$pass&sms[0][0]=$mobile&sms[0][1]=$message&sms[0][2]=123456789&sid=$sid";
        $crl = curl_init();
        curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($crl,CURLOPT_URL,$url);
        curl_setopt($crl,CURLOPT_HEADER,0);
        curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($crl,CURLOPT_POST,1);
        curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
        $response = curl_exec($crl);
        curl_close($crl);
    }
}


/* ------------------------------------------------------------------------------
 * NO Cash
 * -----------------------------------------------------------------------------
 * 
 * This function remove the browser cash. When user press the back button then the browser load the last 
 * Visited link from cash. This is a problem becasuse when a user successfully login to your site and
 * press the back button thn it show the login page again. To avoid this problem use this function before load the page
 * 
 * 
 * @author: Masud Rana
 * @email: masudrana.iubat@gamil.com
 * 
 */
if (!function_exists("no_cash"))
{

    function no_cash() {
        header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
    }

}


/* ------------------------------------------------------------------------------
 * Last Query
 * -----------------------------------------------------------------------------
 * 
 * This function is use for print the last query executed by using laravel db or Eloquent
 * 
 * Note: please enable query log before run the query DB::enableQueryLog(); after query is execude call this function
 * 
 * @author: Arnob
 * @email: mdarnob007@gamil.com
 */


if (!function_exists("last_query"))
{

    function last_query() {
        $queries = DB::getQueryLog();
        $last_query = end($queries);

        printIt($last_query);
    }

}




/* ------------------------------------------------------------------------------
 * convertTimeZone
 * -----------------------------------------------------------------------------
 * 
 * 
 */


if (!function_exists("changeDateFormat"))
{

    function changeDateFormat($date, $format = 'Y-m-d') {
        if (!$date)
        {
            return '';
        }
        $fromDate = new DateTime($date);
        return $fromDate->format($format);
    }

}



if (!function_exists("checkIfAnyActiveTokenExist"))
{

    function checkIfAnyActiveTokenExist() {
        $data = $this->CI->forget_password_model->select_where('id', array('email' => $this->email, 'status' => 1));
        if (!is_null($data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}

/* ------------------------------------------------------------------------------
 * Get User image
 * ------------------------------------------------------------------------------
 * 
 * This function returnt the url of current login user image
 * 
 */






if (!function_exists("hash_password"))
{

    function hash_password($password) {
        $options = [
            'cost' => 10,
            'salt' => mcrypt_create_iv(26, MCRYPT_DEV_URANDOM),
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

}

if (!function_exists("ck_password"))
{

    function ck_password($password, $hash) {
        //echo "password: ".$password."<br/>Hash: ".$hash;
        if (password_verify($password, $hash))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}

if (!function_exists("hasPrivilege"))
{

    function hasPrivilege($privilegeId) {
        return \App\Library\PrivilegeCheckLib::checkPrivilege($privilegeId, Session::get('id'));
    }

}

if (!function_exists("get_public_holiday"))
{

    function get_public_holiday() {


        $result = file_get_contents("http://www.timeanddate.com/holidays/bangladesh/");

        $result = entre2v2($result, '<table class="zebra fw tb-cl tb-hover">', '</table>');
        //printIt($result);
        $result = explode('<tbody>', $result);
        //printIt($result);
        $result = explode('<tr', $result[1]);

        $final = array();
        for ($i = 1; $i < sizeof($result); $i++)
        {

            $result2 = explode('<th class="nw" >', $result[$i]);

            $result3[0] = date('Y-m-d', strtotime(explode('</th>', $result2[1])[0]));

            $result3[1] = entre2v2($result2[1], '">', '</a>');
            $result3[2] = entre2v2($result2[1], '</a></td><td>', '</td>');

            if ($result3[2] == 'Public Holiday' or $result3[2] == 'National holiday')
            {
                $final[] = $result3;
            }
        }

        //printIt($final);
        if (isset($final))
        {
            $public_array = array();
            for ($i = 0; $i < sizeof($final); $i++)
            {


                $public_array[$i][0] = NULL;

                $public_array[$i][1] = NULL;

                $public_array[$i][2] = $final[$i][1];

                $public_array[$i][3] = $final[$i][0];
                $public_array[$i][4] = "00:00:00";
                $public_array[$i][5] = $final[$i][0];
                $public_array[$i][6] = "00:00:00";
                $public_array[$i][7] = NULL;
                $public_array[$i][8] = "#ff0000";
            }
        }
        //printIt($public_array);
        if (isset($public_array))
        {
            $public_array = array_values($public_array);
            return $public_array;
        }
        else
        {
            return NULL;
        }
    }

}




if (!function_exists("getCurrentUrl"))
{

    function getCurrentUrl() {

        return Route::getFacadeRoot()->current()->uri();
    }

}





